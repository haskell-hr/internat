    description
        template-haskell code that parses *.msg files in lang/ directory and creates data and functions for internationalization 

    useful library to generate *.msg files from single *.csv file if you don't have *.msg files
        https://github.com/valentinomicudaj/csvtomsg

    example 
        lang/ directory content

            en.msg
                Title: Title
                Message: Hello

            hr.msg
                Title: Naslov
                Message: Bok

            ge.msg
                Title: Titel
                Message: Halo

        code generated with TH from *.msg files

            data Language = En | Hr | Ge

            data TranslationCode = Title | Message 

            translate (IsString t) :: Language -> TranslationCode -> t 
            translate En Title   = "Title" 
            translate En Message = "Hello" 
            translate Hr Title   = "Naslov" 
            translate Hr Message = "Bok" 
            translate Ge Title   = "Titel" 
            translate Ge Message = "Halo" 


    additional functions    
        translateList :: (IsString t) => Language -> [TranslationCode] -> [t]
        translateList lang = fmap (translate lang)

        translateAll :: (IsString t) => Language -> Map TranslationCode t
        translateAll lang = fromList $ fmap ((,) <$> id <*> translate lang) $ enumFromTo minBound maxBound


    trajanje builda  (funkcije buildFromCsvFile za dentum sheet od ~1000 unosa (takoder, slicne vrijednosti za fake podatke generirane iz haksella) )
        'deriving instance Generic' <previse> 
        bez extraFunctionality ~20 s 
        dodavanje svaka od instance Eq, Ord, Show, Read, JSON,... na TranslationCode dodaje par sekunda trajanju builda.
        ukupno sa svime ~45 s

