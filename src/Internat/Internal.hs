{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TupleSections #-}
module Internat.Internal where

import           System.Directory
import           Data.Monoid
import           Data.List
import           Data.Maybe
import           Control.Monad.IO.Class
import           Control.Effects.Logging
import           Data.Text (Text,pack,unpack)
import           Language.Haskell.TH
import           Language.Haskell.TH.Syntax
import           Text.Mustache
import           Data.Map hiding (filter, lookup, null, splitAt)
import           Data.Bifunctor
import qualified Data.Vector as V
import qualified Data.ByteString.Lazy as BS
import qualified Data.Csv as Csv
import           Internat.Misc
import           Data.Aeson hiding(object)
import           Data.Aeson.Types hiding(object)
import           Control.Monad
import           Control.Monad.State.Lazy
import           Text.Read (readMaybe)

fileNamesFromDir :: (MonadIO m, MonadEffect Logging m) => Text -> m ([FilePath],[FilePath])
fileNamesFromDir dir = do
    dirCurr        <- liftIO  getCurrentDirectory
    let dirStr     =  unpack dir
    let dirFullStr =  dirCurr <> "/" <> dirStr
    (liftIO $ doesDirectoryExist dirStr) >>= \case
        False -> error $ "Directory '" <> dirFullStr <> "' doesn't exist"
        True -> do
            logInfo $  "Reading files from dir '" <> pack dirFullStr
            fileNames           <-   fmap (filter (isSuffixOf "msg"))
                                   $ (liftIO $ getDirectoryContents dirStr)
            let fullFileNames   =    fmap ((dirStr<>"/")<>) fileNames

            logInfo $ "lang files: " <> pshow fullFileNames
            return (fileNames, fullFileNames)


getMsgFiles :: (MonadIO m, MonadEffect Logging m) => Text -> m [(Text,Text)]
getMsgFiles dir = do
    (fileNames, fullFileNames) <- fileNamesFromDir dir
    contents  <- mapM (liftIO . readFile) fullFileNames
    logInfo $ "lang files content: " <> pshow contents

    let languages = fmap (languageFromFileName . pack) fileNames
    return $ zip languages (fmap pack contents)


parseMsgFile :: (MonadEffect Logging m) =>  Text -> m [(Text,Text)]
parseMsgFile =  mapM parseLine . fmap pack . filter (elem ':') . lines . unpack

parseLine :: (MonadEffect Logging m) => Text -> m (Text,Text)
parseLine lineText = do
    let line = unpack lineText
    case elemIndex ':' line of
        Nothing -> error "msg file line must have format [key]:[value]"
        Just i  -> do
            let (keyRaw,(_:valRaw)) = splitAt i line
            let (key,val) = cleanKeyVal (pack keyRaw,pack valRaw)
            logInfo $ "parsed line: " <> pshow (key,val)
            return (key,val)


extractMapFromCsvFile :: (MonadIO m) => Text -> m (Map (Text,Text) Text)
extractMapFromCsvFile fileName = do
    liftIO (doesFileExist $ unpack fileName) >>= \case
        False -> error $ "File '" <>  unpack fileName <> "' doesn't exist"
        True  -> do
            csv <- liftIO $ BS.readFile (unpack fileName)
            let decoded = V.toList $ either error id $ Csv.decode Csv.NoHeader csv :: [[Text]]
            if null decoded then error $ "empty file:" <> (unpack fileName)
                else do
                    let languages = fmap cleanLang $ if any null decoded
                        then  error $ "empty row exists in file " <> (unpack fileName)
                        else  tail (head decoded)
                    let keysAndVals  = transpose $ unjagg $ tail decoded
                    let transKeys    = fmap cleanKey $ head keysAndVals
                    let keyDuplicates = duplicates transKeys
                    case keyDuplicates of
                        [] -> do
                            let transVals    = fmap (fmap cleanVal) $ tail keysAndVals
                            let codeMaps     = fmap (zip transKeys) transVals
                            return $ langAndCodeMapsToTransMap languages codeMaps
                        _  -> error $ "duplicate keys: " <> show keyDuplicates


-- use second line for detailed output
logHandler :: (Monad m) => RuntimeImplemented Logging m a -> m a
logHandler = muteLogs
-- logHandler = prettyPrintSummary

langDataName     = mkName "Language"          :: Name
codeDataName     = mkName "TranslationCode"   :: Name
transFuncName    = mkName "translateCode"     :: Name

extraFunctionality :: Quasi m => m [Dec]
extraFunctionality = runQ
    [d|
    deriving instance Eq      $(conT langDataName)
    deriving instance Ord     $(conT langDataName)
    deriving instance Show    $(conT langDataName)
    deriving instance Read    $(conT langDataName)
    deriving instance Bounded $(conT langDataName)
    deriving instance Enum    $(conT langDataName)
    instance FromJSON         $(conT langDataName) where parseJSON = readSafe <=< (parseJSON :: Value -> Parser String)

    instance ToJSON           $(conT langDataName) where toJSON = toJSON . show
    instance ToJSONKey        $(conT langDataName)
    instance FromJSONKey      $(conT langDataName)
    instance ToMustache       $(conT langDataName) where toMustache = toMustache . toJSON

    readCaseInvariant :: String -> $(conT langDataName)
    readCaseInvariant = read . cleanLang'

    readMaybeCaseInvariant :: String -> Maybe $(conT langDataName)
    readMaybeCaseInvariant = readMaybe . cleanLang'


    deriving instance Eq      $(conT codeDataName)
    deriving instance Ord     $(conT codeDataName)
    deriving instance Show    $(conT codeDataName)
    deriving instance Read    $(conT codeDataName)
    deriving instance Bounded $(conT codeDataName)
    deriving instance Enum    $(conT codeDataName)
    instance FromJSON         $(conT codeDataName) where parseJSON = readSafe <=< (parseJSON :: Value -> Parser String)
    instance ToJSON           $(conT codeDataName) where toJSON = toJSON . show
    instance ToJSONKey        $(conT codeDataName)
    instance FromJSONKey      $(conT codeDataName)
    instance ToMustache       $(conT codeDataName) where toMustache = toMustache . toJSON

    instance ToMustache (Map $(conT codeDataName) Text) where
        toMustache =  object
                    . fmap (uncurry (~>))
                    . fmap (bimap (pack . show) toMustache)
                    . toList


    class Translatable a where
        type TranslatesTo a :: *
        translate :: $(conT langDataName) -> a -> TranslatesTo a

    translateDefault :: $(conT langDataName) -> $(conT langDataName) -> $(conT codeDataName) -> Text
    translateDefault defLang lang code = fromMaybe en (translateCode lang code)
        where en = fromMaybe (error $ "Translation for "
                                   <> show code
                                   <> " doesn't exist even in english")
                             (translateCode defLang code)


    instance Translatable $(conT codeDataName) where
        type TranslatesTo $(conT codeDataName) = Maybe Text
        translate = translateCode

    translateList :: (Translatable a, Functor f) => $(conT langDataName) -> f a -> f (TranslatesTo a)
    translateList lang = fmap (translate lang)

    translateAll :: $(conT langDataName) -> Map $(conT codeDataName) Text
    translateAll lang = fromList
        $ fmap (bimap id fromJust)
        $ filter (isJust . snd)
        $ fmap ((,) <$> id <*> translateCode lang)
        $ enumFromTo minBound maxBound

    translateDefaultAll :: $(conT langDataName) -> $(conT langDataName) -> Map $(conT codeDataName) Text
    translateDefaultAll defLang lang = fromList
        $ fmap ((,) <$> id <*> translateDefault defLang lang)
        $ enumFromTo minBound maxBound
    |]



buildFromMsgFiles :: (Quasi  m) => Text ->  m [Dec]
buildFromMsgFiles msgDir = do
    (_,fullFileNames) <- qRunIO $ logHandler $ fileNamesFromDir msgDir
    unQ $ mapM_ addDependentFile fullFileNames
    (languages,contents)  <- fmap unzip (qRunIO $ logHandler $ getMsgFiles msgDir)     :: (Quasi m) => m ([Text], [Text] )
    codeMaps              <- qRunIO $ logHandler $ mapM parseMsgFile contents          :: (Quasi m) => m [[(Text,Text)]]
    let keyss = fmap (fmap fst) codeMaps
    let duplicateKeyss = fmap duplicates keyss
    case all null duplicateKeyss of
        True -> buildFromMap $ langAndCodeMapsToTransMap languages codeMaps
        False -> error $ "duplicate keys exist in some language: " <> show (zip languages duplicateKeyss)


buildFromCsvFile :: (Quasi m) => Text -> m [Dec]
buildFromCsvFile fileName = do
    unQ $ addDependentFile $ unpack fileName
    transMap <- qRunIO $ logHandler $ extractMapFromCsvFile fileName                   :: (Quasi m) => m (Map (Text,Text) Text)

    buildFromMap transMap


buildFromFakeData :: (Quasi m) => Int -> Int -> m [Dec]
buildFromFakeData nbrOfLanguages nbrOfTransCodes= do
    let languages  = fmap (("Lang"<>) . pack . show) [0..nbrOfLanguages-1]
    let transCodes = fmap (("Code"<>) . pack . show) [0..nbrOfTransCodes-1]
    let value = (<>)
    let codeMapsText  = [[(key,value lang key) | key <- transCodes] | lang <-languages]
    let transMap = fromList $ concat $ zipWith (\lang codeMap -> fmap (\(key,val) -> ((lang,key),val)) codeMap) languages codeMapsText

    buildFromMap transMap

buildFromMap :: (Quasi m) =>  Map (Text,Text) Text -> m [Dec]
buildFromMap transMapText  = do
    let langCodes    = keys transMapText
    let langTexts    = nub $ fmap fst langCodes
    let codeTexts    = nub $ fmap snd langCodes

    let langNames    = fmap (mkName . unpack) langTexts                              :: [Name]
    let langCons     = fmap (flip NormalC []) langNames                              :: [Con]
    let langData     = DataD [] langDataName [] Nothing langCons []                  :: Dec
    let textLangMap  = fromList $ zip langTexts langNames                            :: Map Text Name

    let codeNames    = fmap (mkName . unpack) codeTexts                              :: [Name]
    let codeCons     = fmap (flip NormalC []) codeNames                              :: [Con]
    let codeData     = DataD [] codeDataName [] Nothing codeCons []                  :: Dec
    let textCodeMap  = fromList $ zip codeTexts codeNames                            :: Map Text Name

    let transMap     = mapKeys (bimap (textLangMap !) (textCodeMap !)) transMapText  :: Map (Name,Name) Text
    let transMapList = toList transMap

    let transFunc    = FunD transFuncName
         ( [ Clause [ConP langName [], ConP codeName []]
                     (NormalB $ AppE (ConE 'Just) $ LitE $ StringL $ unpack value ) []
            | ((langName,codeName),value)  <- transMapList ]
         <> [Clause [WildP, WildP] (NormalB (ConE 'Nothing)) []] )                    :: Dec


    fmap ([langData, codeData, transFunc] <>) extraFunctionality
