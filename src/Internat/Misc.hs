module Internat.Misc where

import           Data.Char
import           Data.List
import           Data.Text (Text,pack,unpack,strip)
import           Data.Function
import           Data.Monoid
import           Data.Bifunctor
import           Text.Read
import           Data.Map hiding (filter, (\\), take, splitAt)
import qualified Control.Monad.Fail as Fail

pshow :: (Show a) => a -> Text
pshow = pack . show

tst :: (String -> String) -> Text -> Text
tst f = pack . f . unpack

filterSpace :: Text -> Text
filterSpace = tst filterSpace'

upperFirst :: Text -> Text
upperFirst = tst upperFirst'

upperFirstOnly :: Text -> Text
upperFirstOnly = tst upperFirstOnly'

upperFirstWords :: Text -> Text
upperFirstWords = tst upperFirstWords'

languageFromFileName :: Text -> Text
languageFromFileName = cleanLang . tst removeFileExtension



filterSpace' :: String -> String
filterSpace' = filter (not . isSpace)

upperFirst' :: String -> String
upperFirst' ""  = ""
upperFirst' (x:xs) = toUpper x : xs

upperFirstOnly' :: String -> String
upperFirstOnly' = upperFirst' . fmap toLower


upperFirstWords' :: String -> String
upperFirstWords' = unwords . fmap upperFirst' . words

cleanLang' = upperFirstOnly'

cleanKeyVal :: (Text,Text) -> (Text,Text)
cleanKeyVal = bimap cleanKey cleanVal
cleanKey, cleanVal, cleanLang :: Text -> Text
cleanKey = filterSpace . upperFirstWords
cleanVal = strip
cleanLang = upperFirstOnly

removeFileExtension :: String -> String
removeFileExtension fileName =
    case elemIndex '.' fileName of
        Nothing -> error "msg filename shoud have format [language].[extension]"
        Just i  -> fst $ splitAt i fileName

-- converts
-- [["1"]
-- ,["2","3"]] to
-- [["1",""]
-- ,["2","3"]]
-- so x = transpose (transpose x)
unjaggWith :: a -> [[a]] -> [[a]]
unjaggWith _ []  = []
unjaggWith x xss = let maxLen = length $ maximumBy (compare `on` length) xss
    in fmap (take maxLen . (<> repeat x) ) xss

unjagg :: (Monoid a) => [[a]] -> [[a]]
unjagg = unjaggWith mempty


readSafe :: (Read a,Fail.MonadFail m) => String -> m a
readSafe s = either (Fail.fail . (("Failed to read '" <> s <> "'. ") <> )) return
           $ readEither s

langAndCodeMapsToTransMap :: [Text] -> [[(Text,Text)]] -> Map (Text,Text) Text
langAndCodeMapsToTransMap languages codeMaps = fromList
                          $ concat
                          $ zipWith
                          (\lang codeMap -> fmap (\(key,val) -> ((lang,key),val)) codeMap) languages codeMaps

duplicates :: (Eq a) => [a] -> [a]
duplicates = (\\) <$> id <*> nub
