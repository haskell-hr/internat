{-# LANGUAGE OverloadedStrings, DeriveFunctor #-}
module Internat.IPCountry where

import Data.Vector (Vector, fromList, thaw, (!))
import qualified Data.Text.IO as T
import qualified Data.Text as T
import Data.Text (Text)
import Control.Monad.IO.Class
import Data.Function
import Data.Word
import Data.Vector.Algorithms.Search
import Control.Monad.ST
import Data.Map (Map)
import qualified Data.Map as Map

data Block a = Block { address     :: a
                     , countryCode :: Int }
    deriving (Eq, Ord, Read, Show, Functor)

data IpAddress = IpAddress Word8 Word8 Word8 Word8
    deriving (Eq, Ord, Read, Show)

data Country = Country { isoCode     :: Text
                       , countryName :: Text }

loadBlocks :: MonadIO m => Text -> m (Vector (Block Text))
loadBlocks path = do
    file <- liftIO $ T.readFile (T.unpack path)
    file & T.lines
         & drop 1
         & fmap (\line ->
            let (i : c : _) = T.splitOn "," line
            in Block i (read (T.unpack c)))
         & fromList
         & return

textToIp :: Text -> IpAddress
textToIp txt = IpAddress a' b' c' d'
  where
    [a, b, c, d] = T.splitOn "." txt
    [a', b', c', d'] = map (read . T.unpack) [a, b, c, head (T.splitOn "/" d)]

toSearchable :: Vector (Block Text) -> Vector (Block IpAddress)
toSearchable = fmap (fmap textToIp)

loadLookup :: MonadIO m => Text -> m (Vector (Block IpAddress))
loadLookup = fmap toSearchable . loadBlocks

findCountryCode :: IpAddress -> Vector (Block IpAddress) -> Int
findCountryCode addr vec = runST $ do
    v <- thaw (fmap address vec)
    idx <- binarySearch v addr
    return (countryCode (vec ! (idx - 1)))

loadCountries :: MonadIO m => Text -> m (Map Int Country)
loadCountries path = do
    file <- liftIO $ T.readFile (T.unpack path)
    file & T.lines
         & drop 1
         & fmap (\line ->
            let spl = T.splitOn "," line
                [code, _, _, _, iso, name] = take 5 spl ++ [T.concat (drop 5 spl)]
            in (read (T.unpack code), Country iso name))
         & Map.fromList
         & return

lookupCountry :: Vector (Block IpAddress) -> Map Int Country -> IpAddress -> Country
lookupCountry v m addr = m Map.! findCountryCode addr v
